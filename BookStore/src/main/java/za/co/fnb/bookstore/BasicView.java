/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.fnb.bookstore;
/**
 *
 * @author Samuel Phatlane
 */
/*
@ManagedBean(name="dtBasicView")
@ViewScoped
*/
public class BasicView
{
    private String bookId;
    private String bookTitle;
    private String bookYear;
    private double bookPrice;
    private String categoryDesc;
    private String bookAuthor;

    public BasicView(){
        
    }
    public BasicView(String id, String title, String year, double price, String category, String author)
    {
        this.bookId = id;
        this.bookTitle = title;
        this.bookPrice = price;
        this.categoryDesc = category;
        this.bookAuthor = author;
    }
    
    
    public void setBookId(String bookId) {
        this.bookId = bookId;
    }
    public String getBookId() {
        return bookId;
    }
    
    
    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }
    public String getBookTitle() {
        return bookTitle;
    }
    
    public void setBookYear(String bookYear) {
        this.bookYear = bookYear;
    }
    public String getBookYear() {
        return bookYear;
    }

    public void setBookPrice(double bookPrice) {
        this.bookPrice = bookPrice;
    }
    public double getBookPrice() {
        return bookPrice;
    }
    
    
    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }
    public String getCategoryDesc() {
        return categoryDesc;
    }

    
    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }
    public String getBookAuthor() {
        return bookAuthor;
    }

    
}
