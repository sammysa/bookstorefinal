/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package za.co.fnb.bookstore;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Samuel Phatlane
 */


@ManagedBean
@SessionScoped
public class BookBean implements Serializable{
    public List<BasicView> getBook() throws ClassNotFoundException, SQLException
    {
        Connection connect = null;
        String url = "jdbc:mysql://196.46.186.134:3306/prologiq_jdbc_test";
        String username = "prologiq_jadmin";
        String password = "@PrologiQ29";
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            connect = DriverManager.getConnection(url, username, password);
        }catch(SQLException exept)
        {
            System.out.println("in exec");
            System.out.println(exept.getMessage());
        }
        
        List<BasicView> books = new ArrayList<BasicView>();
	PreparedStatement bookDetails = connect.prepareStatement("SELECT id, title, year, price FROM book");
        PreparedStatement bookCategory = connect.prepareStatement("SELECT category.description where book.category_id = category.id");
        PreparedStatement bookAuthor = connect.prepareStatement("SELECT authors.author_name where book.id = authors.book_id");
        
	ResultSet bkDetails = bookDetails.executeQuery();
        ResultSet bkCategory = bookCategory.executeQuery();
        ResultSet bkAuthor = bookAuthor.executeQuery();
        
        while (bkDetails.next()) 
        {
            BasicView basicView = new BasicView();
                
            basicView.setBookId(bkDetails.getString("id"));
            basicView.setBookTitle(bkDetails.getString("title"));
            basicView.setBookYear(bkDetails.getString("year"));
            basicView.setBookPrice(bkDetails.getDouble("price"));
                
            basicView.setCategoryDesc(bkCategory.getString("description"));
            
            basicView.setCategoryDesc(bkAuthor.getString("author_name"));
            //bookView.setMfdctry(rs.getString("Manufactured_Country"));

            books.add(basicView);

        }

	// close resources
	bkDetails.close();
	bookDetails.close();
        bkCategory.close();
	bookCategory.close();
        bkAuthor.close();
	bookAuthor.close();
	connect.close();
        
        return books;
    }
}
